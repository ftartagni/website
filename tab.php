<style>
#refresh {
  margin-top: 5%;
  margin-bottom: 2%;
}
#navbar {
  font-weight: bold;
  font-size: 38px;
}
  @media only screen and (min-width: 1000px) {
    .table-responsive{
      width: 80%;
      margin-left: 10%;
      margin-right: 10%;
    }
  }
</style>
<div class="col xl-6">
<div class="table-responsive">
  <button id="refresh" type="button" class="btn btn-success btn-lg">
    <i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>
    <table id="tabbb" class="table table-striped">
        <thead class="thead-dark">
          <tr>
            <th id="timeStart" width="18%" scope="col">DATE TIME START</th>
            <th id="timeEnd" width="18%" scope="col">DATE TIME END</th>
            <th id="tempStart" width="10%" scope="col"> INITIAL °C</th>
            <th id="tempEnd" width="10%" scope="col">FINAL °C</th>
            <th id="humStart" width="12%" scope="col">INITIAL HUMIDITY</th>
            <th id="humEnd" width="10%" scope="col">FINAL HUMIDITY</th>
            <th id="id" width="5%" scope="col">ID</th>
            <!-- <th id="roomB" width="5%" scope="col">ROOM B</th> -->
            <!-- <th id="note" width="30%" scope="col">NOTE</th> -->
          </tr>
        </thead>
        <tbody>
          <tr>
            <?php foreach ($select as $row): ?>
              <td headers="timeStart"><?php echo($row['time_start']); ?></td>
              <td headers="timeEnd"><?php echo(is_null($row['time_end']) ? "In corso ..." : $row['time_end']); ?></td>
              <td headers="tempStart"><?php echo($row['temp_start']); ?></td>
              <td headers="tempEnd"><?php echo(is_null($row['temp_end']) ? "In corso ..." : $row['temp_end']); ?></td>
              <td headers="humStart"><?php echo($row['hum_start']); ?></td>
              <td headers="humEnd"><?php echo(is_null($row['hum_end']) ? "In corso ..." : $row['hum_end']); ?></td>
              <td headers="id"><?php echo($row['id']); ?></td>
              <!-- <td headers="roomB"> Prova.</td> -->
          </tr>
              <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>
