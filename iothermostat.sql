-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mag 31, 2021 alle 09:25
-- Versione del server: 10.1.36-MariaDB
-- Versione PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iothermostat`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `thermo_log`
--

CREATE TABLE `thermo_log` (
  `id` int(3) NOT NULL,
  `temp_start` float NOT NULL,
  `temp_end` float DEFAULT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime DEFAULT NULL,
  `hum_start` float NOT NULL,
  `hum_end` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `thermo_log`
--

INSERT INTO `thermo_log` (`id`, `temp_start`, `temp_end`, `time_start`, `time_end`, `hum_start`, `hum_end`) VALUES
(1, 19.9, 24.5, '2021-05-18 10:53:13', '2021-05-19 16:20:25', 0, 0),
(2, 24.5, 24.5, '2021-05-19 18:44:48', '2021-05-19 18:45:00', 0, 0),
(3, 24.2, 25.6, '2021-05-22 10:21:56', '2021-05-22 10:23:14', 0, 0),
(4, 23.7, 24.1, '2021-05-25 12:01:05', '2021-05-25 12:03:21', 47, 48),
(5, 23.8, 24.7, '2021-05-26 11:07:58', '2021-05-26 11:10:34', 51, 53);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `thermo_log`
--
ALTER TABLE `thermo_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `thermo_log`
--
ALTER TABLE `thermo_log`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
